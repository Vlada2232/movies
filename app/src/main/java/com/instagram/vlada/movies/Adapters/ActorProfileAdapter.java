package com.instagram.vlada.movies.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.KnownFor;
import com.instagram.vlada.movies.model.Movie;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ActorProfileAdapter extends RecyclerView.Adapter<ActorProfileAdapter.ViewHolder> {

    private Context context;
    private List<KnownFor> knownForList;

    public ActorProfileAdapter(Context context, List<KnownFor> knownForList) {
        this.context = context;
        this.knownForList = knownForList;
    }

    @Override
    public ActorProfileAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View view = inflator.inflate(R.layout.list_actor_known_for, null);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(ActorProfileAdapter.ViewHolder holder, final int position) {
        if (knownForList.get(position).getMedia_type().equals("movie")){
            holder.name.setText(knownForList.get(position).getTitle());
        }
        if (knownForList.get(position).getMedia_type().equals("tv")){
            holder.name.setText(knownForList.get(position).getName());
        }

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.onClick(knownForList.get(position));
            }
        });

        Picasso.get().load(Constants.BASE_IMAGE_URL + knownForList.get(position).getPoster_path()).into(holder.image);

    }

    @Override
    public int getItemCount() {
        return knownForList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private ImageView image;
        private LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            image = itemView.findViewById(R.id.image);
            linearLayout = itemView.findViewById(R.id.linearLayout);
        }
    }

    private onItemClickListener onItemClickListener;
    public interface onItemClickListener {

        void onClick (KnownFor knownFor);

    }

    public void setOnItemClickListener(ActorProfileAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
