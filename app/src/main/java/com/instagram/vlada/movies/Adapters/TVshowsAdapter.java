package com.instagram.vlada.movies.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Genre;
import com.instagram.vlada.movies.model.TVShow;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class TVshowsAdapter extends RecyclerView.Adapter<TVshowsAdapter.ViewHolder> {

    private Context context;
    private List<TVShow> tvShows;
    private List<Genre> genres;

    public TVshowsAdapter(Context context, List<TVShow> tvShows, List<Genre> genres) {
        this.context = context;
        this.tvShows = tvShows;
        this.genres = genres;
    }

    @Override
    public TVshowsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.list_adapter_view, null);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(TVshowsAdapter.ViewHolder holder, int position) {
        final TVShow tvShow = tvShows.get(position);

//        List<Double> genreIdList = tvShow.getGenre_ids();
//        List<String> genreNames = new ArrayList<>();
//        for (double id : genreIdList){
//            for (Genre genre : genres){
//                if (id == genre.getId()){
//                    genreNames.add(genre.getName());
//                }
//            }
//        }
//
//        StringBuilder builder = new StringBuilder();
//        for (String name: genreNames) {
//            builder.append(name + " ");
//        }

        DateFormat outputFormat = new SimpleDateFormat("yyyy", Locale.US);
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

        String inputText = tvShow.getFirst_air_date();
        Date date = null;
        try {
            date = inputFormat.parse(inputText);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String outputText = outputFormat.format(date);

        holder.TVShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.OnItemClick(tvShow);
            }
        });

        holder.releaseDate.setText(outputText);

        holder.TVShowName.setText(tvShow.getOriginal_name());
        holder.TVShowName.setTextColor(Color.WHITE);


        holder.TVShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClickListener.OnItemClick(tvShow);
            }
        });

        Picasso.get().load(Constants.BASE_IMAGE_URL + tvShow.getPoster_path()).into(holder.TVShowImage);

    }

    @Override
    public int getItemCount() {
        return tvShows.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView TVShowImage;
        TextView TVShowName, releaseDate;
        LinearLayout TVShow;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            TVShowName = itemView.findViewById(R.id.name);
            releaseDate = itemView.findViewById(R.id.release_date);
            TVShow = itemView.findViewById(R.id.linearLayout);
            TVShowImage = itemView.findViewById(R.id.image);
        }
    }

    private onItemClickListener onItemClickListener;
    public interface onItemClickListener{

        void OnItemClick(TVShow tvShow);

    }

    public void setOnItemClickListener(onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }
}
