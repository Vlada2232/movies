package com.instagram.vlada.movies.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.instagram.vlada.movies.Adapters.TVshowsAdapter;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.Adapters.MovieAdapter;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.Utils.GridItemSpacingDecoration;
import com.instagram.vlada.movies.model.Genre;
import com.instagram.vlada.movies.model.GenreResponse;
import com.instagram.vlada.movies.model.Movie;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.model.MovieResponse;
import com.instagram.vlada.movies.model.TVShow;
import com.instagram.vlada.movies.model.TVShowResponse;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Movies extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ImageView back;
    private MovieAdapter adapter;
    private TVshowsAdapter tVshowsAdapter;
    private List<Movie> movies = new ArrayList<>();
    private List<TVShow> tvShows;
    private List<Genre> genres;
    private int page = 1;
    private Endpoints endpoints;
    private Button discoverButton, moviesButton, tvshowButton, favouriteButton;
    private TextView discoverText, moviesText, tvshowText, favouriteText;
    private TabLayout tabLayoutMovies, tabLayoutTVShows;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movies);

        endpoints = ApiUtils.getApiService();
        tabLayoutMovies = findViewById(R.id.tab_layoutMovies);
        tabLayoutTVShows = findViewById(R.id.tab_layoutTVShows);
        tabLayoutTVShows.setVisibility(View.GONE);
        Buttons();
        movieTab();
        tvShowTab();
        getPopular();

        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(getResources().getColor(R.color.black));

        favouriteButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
        favouriteText.setTextColor(Color.WHITE);
        discoverButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
        discoverText.setTextColor(Color.WHITE);
        moviesButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.black));
        moviesText.setTextColor(Color.BLACK);
        tvshowButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
        tvshowText.setTextColor(Color.WHITE);


        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(Movies.this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridItemSpacingDecoration(3, 30, true));

   }


    private void getPopular(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getFavouriteMovies(Constants.API_KEY, "en-US", String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                if (response.isSuccessful() && response.body() != null){
                    movies = response.body().getResults();
                    if (call.isExecuted()){
                        progressDialog.dismiss();
                    }
                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                adapter = new MovieAdapter(Movies.this, movies, genres);
                                adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(Movie movie) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "Movie");
                                        intent.putExtra("Movie", movie);
                                        startActivity(intent);
                                    }
                                });

                                adapter.setLoadMore(new MovieAdapter.loadMore() {
                                    @Override
                                    public void load() {
                                        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");
                                        page = page + 1;
                                        endpoints.getFavouriteMovies(Constants.API_KEY, "en-US",String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
                                            @Override
                                            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                                                if (response.isSuccessful() && response.body().getResults() != null){
                                                    movies.addAll(movies.size(), response.body().getResults());
                                                    adapter.notifyDataSetChanged();
                                                    adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                                        @Override
                                                        public void OnItemClick(Movie movie) {
                                                            Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                                            intent.putExtra("whatScreen", "Movie");
                                                            intent.putExtra("movie", movie);
                                                            startActivity(intent);
                                                        }
                                                    });
                                                    progressDialog.dismiss();
                                                    if (page == Integer.valueOf(response.body().getTotal_pages())){
                                                        adapter.hideLoadMore(true);
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                                recyclerView.setAdapter(adapter);
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                            else {
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
                else {
                    progressDialog.dismiss();
                }
            }
            @Override
            public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                if (call.isExecuted() || call.isCanceled()){
                    progressDialog.dismiss();
                }
            }
        });

    }

    private void getTopRated(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getTopRatedMovies(Constants.API_KEY, "en-US", String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
                @Override
                public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                    if (response.isSuccessful() && response.body().getResults() != null){
                        movies = response.body().getResults();
                        if (call.isExecuted()){
                            progressDialog.dismiss();
                        }
                        endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                                if (response.isSuccessful() && response.body().getGenres() != null){
                                    genres = response.body().getGenres();
                                    adapter = new MovieAdapter(Movies.this, movies, genres);
                                    adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                        @Override
                                        public void OnItemClick(Movie movie) {
                                            Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                            intent.putExtra("whatScreen", "Movie");
                                            intent.putExtra("Movie", movie);
                                            startActivity(intent);
                                        }
                                    });

                                    adapter.setLoadMore(new MovieAdapter.loadMore() {
                                        @Override
                                        public void load() {
                                            final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");
                                            page = page + 1;
                                            endpoints.getFavouriteMovies(Constants.API_KEY, "en-US",String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
                                                @Override
                                                public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                                                    if (response.isSuccessful() && response.body().getResults() != null){
                                                        movies.addAll(movies.size(), response.body().getResults());
                                                        adapter.notifyDataSetChanged();
                                                        progressDialog.dismiss();
                                                        if (page == Integer.valueOf(response.body().getTotal_pages())){
                                                            adapter.hideLoadMore(true);
                                                        }
                                                    }
                                                }

                                                @Override
                                                public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                                                    progressDialog.dismiss();
                                                }
                                            });
                                        }
                                    });

                                    recyclerView.setAdapter(adapter);
                                    if (call.isExecuted()){
                                        progressDialog.dismiss();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(Call<GenreResponse> call, Throwable t) {
                                progressDialog.dismiss();
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<MovieResponse> call, Throwable t) {
                    progressDialog.dismiss();
                }
            });



    }

    private void getLatest(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getLatestMovies(Constants.API_KEY, "en-US").enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                if (response.isSuccessful() && response.body() != null){
                    if (response.body().getResults() == null || response.body().getResults().size() == 0 || response.body().getResults().isEmpty()){
                        Toast.makeText(Movies.this, "List is empty", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                    else {
                        if (call.isExecuted()){
                            progressDialog.dismiss();
                        }
                        endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                                if (response.isSuccessful() && response.body().getGenres() != null){
                                    genres = response.body().getGenres();
                                    adapter = new MovieAdapter(Movies.this, movies, genres);
                                    adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                        @Override
                                        public void OnItemClick(Movie movie) {
                                            Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                            intent.putExtra("whatScreen", "Movie");
                                            intent.putExtra("Movie", movie);
                                            startActivity(intent);
                                        }
                                    });

                                    recyclerView.setAdapter(adapter);
                                    if (call.isExecuted()){
                                        progressDialog.dismiss();
                                    }
                                }
                                else {
                                    if (call.isExecuted()){
                                        progressDialog.dismiss();
                                    }
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                                progressDialog.dismiss();
                            }
                        });
                    }

                }
            }

            @Override
            public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });
    }

    private void getNowPlaying() {

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getNowPlayingMovies(Constants.API_KEY, "en-US", String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                if (response.isSuccessful() && response.body() != null){
                    movies = response.body().getResults();
                    if (call.isExecuted()){
                        progressDialog.dismiss();
                    }
                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                adapter = new MovieAdapter(Movies.this, movies, genres);
                                adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(Movie movie) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "Movie");
                                        intent.putExtra("Movie", movie);
                                        startActivity(intent);
                                    }
                                });

                                adapter.setLoadMore(new MovieAdapter.loadMore() {
                                    @Override
                                    public void load() {
                                        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");
                                        page = page + 1;
                                        endpoints.getNowPlayingMovies(Constants.API_KEY, "en-US",String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
                                            @Override
                                            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                                                if (response.isSuccessful() && response.body().getResults() != null){
                                                    movies.addAll(movies.size(), response.body().getResults());
                                                    adapter.notifyDataSetChanged();
                                                    progressDialog.dismiss();
                                                    if (page == Integer.valueOf(response.body().getTotal_pages())){
                                                        adapter.hideLoadMore(true);
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                                recyclerView.setAdapter(adapter);
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                            else {
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });

    }

    private void getUpcoming() {

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, String.valueOf(page), "Loading. Please wait...");

        endpoints.getUpcomingMovies(Constants.API_KEY, "en-US", String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if (response.isSuccessful() && response.body() != null){
                    movies = response.body().getResults();
                    if (call.isExecuted()){
                        progressDialog.dismiss();
                    }
                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                adapter = new MovieAdapter(Movies.this, movies, genres);
                                adapter.setOnItemClickListener(new MovieAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(Movie movie) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "Movie");
                                        intent.putExtra("Movie", movie);
                                        startActivity(intent);
                                    }
                                });

                                adapter.setLoadMore(new MovieAdapter.loadMore() {
                                    @Override
                                    public void load() {
                                        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");
                                        page = page + 1;
                                        endpoints.getUpcomingMovies(Constants.API_KEY, "en-US",String.valueOf(page), "").enqueue(new Callback<MovieResponse>() {
                                            @Override
                                            public void onResponse(@NonNull Call<MovieResponse> call, @NonNull Response<MovieResponse> response) {
                                                if (response.isSuccessful() && response.body().getResults() != null){
                                                    movies.addAll(movies.size(), response.body().getResults());
                                                    adapter.notifyDataSetChanged();
                                                    progressDialog.dismiss();
                                                    if (page == Integer.valueOf(response.body().getTotal_pages())){
                                                        adapter.hideLoadMore(true);
                                                    }
                                                }
                                            }

                                            @Override
                                            public void onFailure(@NonNull Call<MovieResponse> call, @NonNull Throwable t) {
                                                progressDialog.dismiss();
                                            }
                                        });
                                    }
                                });

                                recyclerView.setAdapter(adapter);
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                            else {
                                if (call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });

    }

    @SuppressLint("NewApi")
    private void Buttons(){

        discoverButton = findViewById(R.id.discoverIcon);
        favouriteButton = findViewById(R.id.favouritesIcon);
        moviesButton = findViewById(R.id.moviesIcon);
        tvshowButton = findViewById(R.id.tvshowsIcon);
        discoverText = findViewById(R.id.discoverText);
        favouriteText = findViewById(R.id.favouritesText);
        moviesText = findViewById(R.id.moviesText);
        tvshowText = findViewById(R.id.tvshowsText);

        discoverButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouriteButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                favouriteText.setTextColor(Color.WHITE);
                discoverButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.black));
                discoverText.setTextColor(Color.BLACK);
                moviesButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                moviesText.setTextColor(Color.WHITE);
                tvshowButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                tvshowText.setTextColor(Color.WHITE);

            }
        });

        moviesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPopular();
                tabLayoutTVShows.setVisibility(View.GONE);
                tabLayoutMovies.setVisibility(View.VISIBLE);
                favouriteButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                favouriteText.setTextColor(Color.WHITE);
                discoverButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                discoverText.setTextColor(Color.WHITE);
                moviesButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.black));
                moviesText.setTextColor(Color.BLACK);
                tvshowButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                tvshowText.setTextColor(Color.WHITE);
            }
        });

        tvshowButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPopulatTVShows();
                tabLayoutTVShows.setVisibility(View.VISIBLE);
                tabLayoutMovies.setVisibility(View.GONE);
                favouriteButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                favouriteText.setTextColor(Color.WHITE);
                discoverButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                discoverText.setTextColor(Color.WHITE);
                moviesButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                moviesText.setTextColor(Color.WHITE);
                tvshowButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.black));
                tvshowText.setTextColor(Color.BLACK);
            }
        });

        favouriteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                favouriteButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.black));
                favouriteText.setTextColor(Color.BLACK);
                discoverButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                discoverText.setTextColor(Color.WHITE);
                moviesButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                moviesText.setTextColor(Color.WHITE);
                tvshowButton.setBackgroundTintList(Movies.this.getResources().getColorStateList(R.color.white));
                tvshowText.setTextColor(Color.WHITE);
            }
        });

    }

    private void movieTab(){

        tabLayoutMovies.addTab(tabLayoutMovies.newTab().setText("Popular"));
        tabLayoutMovies.addTab(tabLayoutMovies.newTab().setText("Top Rated"));
        tabLayoutMovies.addTab(tabLayoutMovies.newTab().setText("Latest"));
        tabLayoutMovies.addTab(tabLayoutMovies.newTab().setText("Now Playing"));
        tabLayoutMovies.addTab(tabLayoutMovies.newTab().setText("Upcoming"));
        tabLayoutMovies.setTabTextColors(ContextCompat.getColor(Movies.this, R.color.white), ContextCompat.getColor(Movies.this, R.color.black));

        tabLayoutMovies.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        getPopular();
                        break;
                    case 1:
                        getTopRated();
                        break;
                    case 2:
                        getLatest();
                        break;
                    case 3:
                        getNowPlaying();
                        break;
                    case 4:
                        getUpcoming();
                        break;
                    case 5:
                        getUpcoming();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void tvShowTab(){

        tabLayoutTVShows.addTab(tabLayoutTVShows.newTab().setText("Popular"));
        tabLayoutTVShows.addTab(tabLayoutTVShows.newTab().setText("Top Rated"));
        tabLayoutTVShows.addTab(tabLayoutTVShows.newTab().setText("Latest"));
        tabLayoutTVShows.addTab(tabLayoutTVShows.newTab().setText("Airing Today"));
        tabLayoutTVShows.addTab(tabLayoutTVShows.newTab().setText("On The Air"));
        tabLayoutTVShows.setTabTextColors(ContextCompat.getColor(Movies.this, R.color.white), ContextCompat.getColor(Movies.this, R.color.black));

        tabLayoutTVShows.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                switch (tab.getPosition()) {
                    case 0:
                        getPopulatTVShows();
                        break;
                    case 1:
                        getTopRatedTVShows();
                        break;
                    case 2:
                        getlatestTVShows();
                        break;
                    case 3:
                        getAiringTodayTVShows();
                        break;
                    case 4:
                        getOnTheAirTVShows();
                        break;
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }

    private void getPopulatTVShows(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getPopularTVShows(Constants.API_KEY, "en-US", "1", "").enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(@NonNull Call<TVShowResponse> call, @NonNull Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){

                    tvShows = response.body().getResults();

                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                tVshowsAdapter = new TVshowsAdapter(Movies.this, tvShows, genres);
                                recyclerView.setAdapter(tVshowsAdapter);
                                progressDialog.dismiss();
                                tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(TVShow tvShow) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "TvShow");
                                        intent.putExtra("TvShow", tvShow);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onFailure(@NonNull Call<TVShowResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    private void getTopRatedTVShows(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getTopRatedTVShows(Constants.API_KEY, "en-US", String.valueOf(page)).enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(Call<TVShowResponse> call, Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){

                    tvShows = response.body().getResults();

                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                tVshowsAdapter = new TVshowsAdapter(Movies.this, tvShows, genres);
                                recyclerView.setAdapter(tVshowsAdapter);
                                progressDialog.dismiss();
                                tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(TVShow tvShow) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "TvShow");
                                        intent.putExtra("TvShow", tvShow);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<TVShowResponse> call, Throwable t) {

            }
        });

    }

    private void getlatestTVShows(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getlatestTVShows(Constants.API_KEY, "em-US").enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(Call<TVShowResponse> call, Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){
                    if (response.body().getResults() == null || response.body().getResults().size() == 0 || response.body().getResults().isEmpty()){
                        Toast.makeText(Movies.this, "List is empty", Toast.LENGTH_LONG).show();
                        progressDialog.dismiss();
                    }
                    else{

                        tvShows = response.body().getResults();

                        endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                            @Override
                            public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                                if (response.isSuccessful() && response.body().getGenres() != null){
                                    genres = response.body().getGenres();
                                    tVshowsAdapter = new TVshowsAdapter(Movies.this, tvShows, genres);
                                    recyclerView.setAdapter(tVshowsAdapter);
                                    progressDialog.dismiss();
                                    tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                        @Override
                                        public void OnItemClick(TVShow tvShow) {
                                            Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                            intent.putExtra("whatScreen", "TvShow");
                                            intent.putExtra("TvShow", tvShow);
                                            startActivity(intent);
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                                progressDialog.dismiss();
                            }
                        });

                    }

                }
                else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<TVShowResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    private void getAiringTodayTVShows(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getAiringTodayTVShows(Constants.API_KEY, "en-US", String.valueOf(page)).enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(Call<TVShowResponse> call, Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){

                    tvShows = response.body().getResults();

                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                tVshowsAdapter = new TVshowsAdapter(Movies.this, tvShows, genres);
                                recyclerView.setAdapter(tVshowsAdapter);
                                progressDialog.dismiss();
                                tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(TVShow tvShow) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "TvShow");
                                        intent.putExtra("TvShow", tvShow);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<TVShowResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }

    private void getOnTheAirTVShows(){

        final ProgressDialog progressDialog = ProgressDialog.show(Movies.this, "", "Loading. Please wait...");

        endpoints.getOnTheAirTVShows(Constants.API_KEY, "en-US", String.valueOf(page)).enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(Call<TVShowResponse> call, Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){

                    tvShows = response.body().getResults();

                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                tVshowsAdapter = new TVshowsAdapter(Movies.this, tvShows, genres);
                                recyclerView.setAdapter(tVshowsAdapter);
                                progressDialog.dismiss();
                                tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(TVShow tvShow) {
                                        Intent intent = new Intent(Movies.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "TvShow");
                                        intent.putExtra("TvShow", tvShow);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onFailure(Call<TVShowResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });

    }
}
