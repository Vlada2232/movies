package com.instagram.vlada.movies.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Genre;
import com.instagram.vlada.movies.model.Movie;
import com.instagram.vlada.movies.R;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private List<Movie> movies;
    private List<Genre> genres;
    private boolean hide;

    public MovieAdapter(Context context, List<Movie> movies, List<Genre> genres) {
        this.context = context;
        this.movies = movies;
        this.genres = genres;
    }

    public void hideLoadMore(boolean hide) {
        this.hide = hide;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        switch (viewType){
            case 0:
                LayoutInflater inflator0 = LayoutInflater.from(context);
                View view = inflator0.inflate(R.layout.list_adapter_view, null);

                return new ViewHolder0(view);

             case 1:
                 LayoutInflater inflator2 = LayoutInflater.from(context);
                 View view2 = inflator2.inflate(R.layout.load_more, null);

                 return new ViewHolder2(view2);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        switch (viewHolder.getItemViewType()) {
            case 0:
                ViewHolder0 viewHolder0 = (ViewHolder0) viewHolder;

                final Movie movie = movies.get(i);

//                List<Double> genreIdList = movie.getGenre_ids();
//                List<String> genreNames = new ArrayList<>();
//                for (double id : genreIdList) {
//                    for (Genre genre : genres) {
//                        if (id == genre.getId()) {
//                            genreNames.add(genre.getName());
//                        }
//                    }
//                }
//
//                StringBuilder builder = new StringBuilder();
//                for (String name : genreNames) {
//                    builder.append(name + " ");
//                }


                viewHolder0.movieName.setText(movie.getTitle());
                viewHolder0.movieName.setTextColor(Color.WHITE);
                DateFormat outputFormat = new SimpleDateFormat("yyyy", Locale.US);
                DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);

                String inputText = movie.getRelease_date();
                Date date = null;
                try {
                    date = inputFormat.parse(inputText);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String outputText = outputFormat.format(date);

                viewHolder0.movie.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        onItemClickListener.OnItemClick(movie);
                    }
                });

                viewHolder0.release_date.setText(outputText);


                Picasso.get().load(Constants.BASE_IMAGE_URL + movie.getPoster_path()).into(viewHolder0.movieImage);

                break;
            case 1:
                ViewHolder2 viewHolder2 = (ViewHolder2) viewHolder;


                if (hide){
                    viewHolder2.more.setVisibility(View.GONE);
                }

                viewHolder2.more.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        loadMore.load();
                    }
                });
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
            return movies.size() == position +1 ? 1 : 0;
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class ViewHolder0 extends RecyclerView.ViewHolder {

        ImageView movieImage;
        TextView movieName, release_date;
        LinearLayout movie;

        ViewHolder0(@NonNull View itemView) {
            super(itemView);

            movieImage = itemView.findViewById(R.id.image);
            movieName = itemView.findViewById(R.id.name);
            release_date = itemView.findViewById(R.id.release_date);
            movie = itemView.findViewById(R.id.linearLayout);

        }
    }

    class ViewHolder2 extends RecyclerView.ViewHolder{

        LinearLayout more;

        ViewHolder2(View itemView) {
            super(itemView);

            more = itemView.findViewById(R.id.more);
        }
    }

    private onItemClickListener onItemClickListener;
    public interface onItemClickListener{

        void OnItemClick(Movie movie);

    }

    public void setOnItemClickListener(MovieAdapter.onItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private loadMore loadMore;
    public interface loadMore {

        void load();

    }

    public void setLoadMore(MovieAdapter.loadMore loadMore) {
        this.loadMore = loadMore;
    }

}
