package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Episode implements Serializable {

    @SerializedName("air_date")
    private String air_date;

    @SerializedName("crew")
    private List crew;

    @SerializedName("episode_number")
    private int episode_number;

    @SerializedName("guest_stars")
    private List<Guest_star> guest_stars;

    @SerializedName("name")
    private String name;

    @SerializedName("overview")
    private String overview;

    @SerializedName("show_id")
    private double show_id;

    @SerializedName("id")
    private double id;

    @SerializedName("production_code")
    private String production_code;

    @SerializedName("season_number")
    private int season_number;

    @SerializedName("still_path")
    private String still_path;

    @SerializedName("vote_average")
    private float vote_average;

    @SerializedName("vote_count")
    private double vote_count;

    public Episode(String air_date, List crew, int episode_number, List<Guest_star> guest_stars, String name, String overview, double show_id, double id, String production_code, int season_number, String still_path, float vote_average, double vote_count) {
        this.air_date = air_date;
        this.crew = crew;
        this.episode_number = episode_number;
        this.guest_stars = guest_stars;
        this.name = name;
        this.overview = overview;
        this.show_id = show_id;
        this.id = id;
        this.production_code = production_code;
        this.season_number = season_number;
        this.still_path = still_path;
        this.vote_average = vote_average;
        this.vote_count = vote_count;
    }

    public String getAir_date() {
        return air_date;
    }

    public void setAir_date(String air_date) {
        this.air_date = air_date;
    }

    public List getCrew() {
        return crew;
    }

    public void setCrew(List crew) {
        this.crew = crew;
    }

    public int getEpisode_number() {
        return episode_number;
    }

    public void setEpisode_number(int episode_number) {
        this.episode_number = episode_number;
    }

    public List<Guest_star> getGuest_stars() {
        return guest_stars;
    }

    public void setGuest_stars(List<Guest_star> guest_stars) {
        this.guest_stars = guest_stars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public double getShow_id() {
        return show_id;
    }

    public void setShow_id(double show_id) {
        this.show_id = show_id;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getProduction_code() {
        return production_code;
    }

    public void setProduction_code(String production_code) {
        this.production_code = production_code;
    }

    public int getSeason_number() {
        return season_number;
    }

    public void setSeason_number(int season_number) {
        this.season_number = season_number;
    }

    public String getStill_path() {
        return still_path;
    }

    public void setStill_path(String still_path) {
        this.still_path = still_path;
    }

    public float getVote_average() {
        return vote_average;
    }

    public void setVote_average(float vote_average) {
        this.vote_average = vote_average;
    }

    public double getVote_count() {
        return vote_count;
    }

    public void setVote_count(double vote_count) {
        this.vote_count = vote_count;
    }
}
