package com.instagram.vlada.movies.Utils;

import com.instagram.vlada.Endpoints;

public class ApiUtils {

    private ApiUtils() {}


    public static final String BASE_URL = "https://api.themoviedb.org/";




    public static Endpoints getApiService() {
        return RetrofitClient.getClient(BASE_URL).create(Endpoints.class);
    }

}
