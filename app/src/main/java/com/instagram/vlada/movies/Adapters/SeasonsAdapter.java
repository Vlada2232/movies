package com.instagram.vlada.movies.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.TVSeason;
import com.squareup.picasso.Picasso;

import java.util.List;

public class SeasonsAdapter extends RecyclerView.Adapter<SeasonsAdapter.ViewHolder>{

    private Context context;
    private List<TVSeason> seasons;

    public SeasonsAdapter(Context context, List<TVSeason> seasons) {
        this.context = context;
        this.seasons = seasons;
    }

    @Override
    public SeasonsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.list_actor_known_for, null);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(SeasonsAdapter.ViewHolder holder, int position) {
        final TVSeason tvSeason = seasons.get(position);

        holder.textView.setText(tvSeason.getName());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(tvSeason.getSeason_number());
            }
        });

        Picasso.get().load(Constants.BASE_IMAGE_URL + tvSeason.getPoster_path()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return seasons.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView;
        TextView textView;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.name);
            imageView = itemView.findViewById(R.id.image);
            linearLayout = itemView.findViewById(R.id.linearLayout);

        }
    }

    private onClickListener onClickListener;
    public interface onClickListener {

        void onClick(int season_number);

    }

    public void setOnClickListener(SeasonsAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
