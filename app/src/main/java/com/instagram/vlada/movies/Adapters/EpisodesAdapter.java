package com.instagram.vlada.movies.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Episode;
import com.squareup.picasso.Picasso;

import java.util.List;

public class EpisodesAdapter extends RecyclerView.Adapter<EpisodesAdapter.ViewHolder> {

    private Context context;
    private List<Episode> episodes;

    public EpisodesAdapter(Context context, List<Episode> episodes) {
        this.context = context;
        this.episodes = episodes;
    }

    @Override
    public EpisodesAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(context);
        View view = inflator.inflate(R.layout.list_adapter_view, null);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(EpisodesAdapter.ViewHolder holder, @SuppressLint("RecyclerView") final int position) {
        final Episode episode = episodes.get(position);

        holder.textView.setText(episode.getName());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onClickListener.onClick(position);
            }
        });
        holder.releaseDate.setText(episode.getAir_date());
        Picasso.get().load(Constants.BASE_IMAGE_URL + episode.getStill_path()).into(holder.imageView);

    }

    @Override
    public int getItemCount() {
        return episodes.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textView, releaseDate;
        LinearLayout linearLayout;

        ViewHolder(View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.image);
            textView = itemView.findViewById(R.id.name);
            linearLayout = itemView.findViewById(R.id.linearLayout);
            releaseDate = itemView.findViewById(R.id.release_date);
        }
    }

    private onClickListener onClickListener;
    public interface onClickListener {

        void onClick (int position);

    }

    public void setOnClickListener(EpisodesAdapter.onClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
