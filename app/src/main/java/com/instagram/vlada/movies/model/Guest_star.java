package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Guest_star implements Serializable{

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("credit_id")
    private String credit_id;

    @SerializedName("character")
    private String character;

    @SerializedName("order")
    private int order;

    @SerializedName("gender")
    private int gender;

    @SerializedName("profile_path")
    private String profile_path;

    public Guest_star(long id, String name, String credit_id, String character, int order, int gender, String profile_path) {
        this.id = id;
        this.name = name;
        this.credit_id = credit_id;
        this.character = character;
        this.order = order;
        this.gender = gender;
        this.profile_path = profile_path;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getCharacter() {
        return character;
    }

    public void setCharacter(String character) {
        this.character = character;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }
}
