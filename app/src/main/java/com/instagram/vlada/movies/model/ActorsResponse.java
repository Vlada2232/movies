package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActorsResponse {

    @SerializedName("page")
    private int page;

    @SerializedName("total_results")
    private String total_results;

    @SerializedName("total_pages")
    private String total_pages;

    @SerializedName("results")
    private List<Actor> results;

    public ActorsResponse(int page, String total_results, String total_pages, List<Actor> results) {
        this.page = page;
        this.total_results = total_results;
        this.total_pages = total_pages;
        this.results = results;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public String getTotal_results() {
        return total_results;
    }

    public void setTotal_results(String total_results) {
        this.total_results = total_results;
    }

    public String getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(String total_pages) {
        this.total_pages = total_pages;
    }

    public List<Actor> getResults() {
        return results;
    }

    public void setResults(List<Actor> results) {
        this.results = results;
    }
}
