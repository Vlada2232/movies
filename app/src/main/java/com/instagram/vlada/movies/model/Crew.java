package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

public class Crew {

    @SerializedName("id")
    private double id;

    @SerializedName("credit_id")
    private String credit_id;

    @SerializedName("name")
    private String name;

    @SerializedName("department")
    private String department;

    @SerializedName("job")
    private String job;

    @SerializedName("profile_path")
    private String profile_path;

    public Crew(double id, String credit_id, String name, String department, String job, String profile_path) {
        this.id = id;
        this.credit_id = credit_id;
        this.name = name;
        this.department = department;
        this.job = job;
        this.profile_path = profile_path;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getCredit_id() {
        return credit_id;
    }

    public void setCredit_id(String credit_id) {
        this.credit_id = credit_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }
}
