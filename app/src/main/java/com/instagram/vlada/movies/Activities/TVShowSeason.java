package com.instagram.vlada.movies.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.Adapters.EpisodesAdapter;
import com.instagram.vlada.movies.Adapters.SeasonsAdapter;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.Utils.GridItemSpacingDecoration;
import com.instagram.vlada.movies.model.Episode;
import com.instagram.vlada.movies.model.TVSeason;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TVShowSeason extends AppCompatActivity {

    private TextView name, air_date, overview, tvshow_name;
    private ImageView imageView;
    private RecyclerView recyclerView;
    private int season_number;
    private double tv_id;
    private EpisodesAdapter adapter;
    private String tvshowName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tvshow_season);

        name = findViewById(R.id.name);
        air_date = findViewById(R.id.air_date);
        overview = findViewById(R.id.overview);
        imageView = findViewById(R.id.image);
        recyclerView = findViewById(R.id.recyclerViewEpisodes);
        tvshow_name = findViewById(R.id.tvshow_name);

        Endpoints endpoints = ApiUtils.getApiService();

        final ProgressDialog progressDialog = ProgressDialog.show(TVShowSeason.this, "", "Loading. Please wait...");

        tv_id = getIntent().getDoubleExtra("tv_id", 0);
        season_number = getIntent().getIntExtra("season_number", 0);
        tvshowName = getIntent().getStringExtra("tvshow_name");

        tvshow_name.setText(tvshowName);

        endpoints.getTvSeason(tv_id, season_number, Constants.API_KEY, "en-US", 1).enqueue(new Callback<TVSeason>() {
            @Override
            public void onResponse(@NonNull Call<TVSeason> call, @NonNull final Response<TVSeason> response) {
                if (response.isSuccessful() && response.body() != null){
                    progressDialog.dismiss();
                    adapter = new EpisodesAdapter(TVShowSeason.this, response.body().getEpisodes());
                    adapter.setOnClickListener(new EpisodesAdapter.onClickListener() {
                        @Override
                        public void onClick(int position) {
                            Episode episode = response.body().getEpisodes().get(position);
                            Intent intent = new Intent(TVShowSeason.this, EpisodeProfile.class);
                            intent.putExtra("episode", episode);
                            intent.putExtra("episode_number", position);
                            startActivity(intent);
                        }
                    });
                    name.setText(response.body().getName());
                    air_date.setText(response.body().getAir_date());
                    overview.setText(response.body().getOverview());

                    Picasso.get().load(Constants.BASE_IMAGE_URL + response.body().getPoster_path()).into(imageView);

                    recyclerView.setAdapter(adapter);
                }
                else {
                    Toast.makeText(TVShowSeason.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<TVSeason> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(TVShowSeason.this, "Something went wrong...", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.setHasFixedSize(true);
        GridLayoutManager layoutManager = new GridLayoutManager(TVShowSeason.this, 3);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.addItemDecoration(new GridItemSpacingDecoration(3, 30, true));

    }

}
