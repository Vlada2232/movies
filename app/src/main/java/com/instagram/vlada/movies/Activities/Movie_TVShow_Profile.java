package com.instagram.vlada.movies.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.Adapters.SeasonsAdapter;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.KnownFor;
import com.instagram.vlada.movies.model.Movie;
import com.instagram.vlada.movies.model.TVSeason;
import com.instagram.vlada.movies.model.TVShow;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Movie_TVShow_Profile extends AppCompatActivity {

    private ImageView imageView;
    private TextView name, rating, release_date, overview;
    private RatingBar ratingBar;
    private Movie movie;
    private KnownFor knownFor;
    private TVShow tvShow;
    private String whatScreen;
    private RecyclerView recyclerView;
    private SeasonsAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_profile);
        imageView = findViewById(R.id.image);
        name = findViewById(R.id.name);
        rating = findViewById(R.id.rating);
        release_date = findViewById(R.id.release_date);
        overview = findViewById(R.id.overview);
        ratingBar = findViewById(R.id.popularityRatingStar);
        ratingBar.setMax(10);
        recyclerView = findViewById(R.id.recyclerViewSeasons);


        knownFor = (KnownFor) getIntent().getSerializableExtra("knownFor");
        movie = (Movie) getIntent().getSerializableExtra("movie");
        tvShow = (TVShow) getIntent().getSerializableExtra("tvshow");

        whatScreen = getIntent().getStringExtra("whatScreen");

        switch (whatScreen){

            case "Movie":
                movie = (Movie) getIntent().getSerializableExtra("Movie");
                getMovie(movie);
                break;

            case "TvShow":
                tvShow = (TVShow) getIntent().getSerializableExtra("TvShow");
                getTvShow(tvShow);
                break;

            case "KnownFor":
                knownFor = (KnownFor) getIntent().getSerializableExtra("KnownFor");
                getKnownFor(knownFor);
                break;
        }

    }

    private void getTvShow(final TVShow tvShow) {

        Picasso.get().load(Constants.BASE_IMAGE_URL + tvShow.getPoster_path()).into(imageView);

        name.setText(tvShow.getOriginal_name());
        rating.setText(String.valueOf(tvShow.getVote_average()));
        release_date.setText(tvShow.getFirst_air_date());
        overview.setText(tvShow.getOverview());

        ratingBar.setRating((float) (tvShow.getPopularity() / 40));

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Movie_TVShow_Profile.this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);

        final Endpoints endpoints = ApiUtils.getApiService();

        endpoints.getTVShow(tvShow.getId(), Constants.API_KEY, "en-US", 1).enqueue(new Callback<TVShow>() {
            @Override
            public void onResponse(Call<TVShow> call, Response<TVShow> response) {
                if (response.isSuccessful() && response.body() != null){
                    List<TVSeason> seasons = response.body().getSeasons();
                    adapter = new SeasonsAdapter(Movie_TVShow_Profile.this, seasons);
                    adapter.setOnClickListener(new SeasonsAdapter.onClickListener() {
                        @Override
                        public void onClick(int season_number) {
                            Intent intent = new Intent(Movie_TVShow_Profile.this, TVShowSeason.class);
                            intent.putExtra("tv_id", tvShow.getId());
                            intent.putExtra("season_number", season_number);
                            intent.putExtra("tvshow_name", tvShow.getName());
                            startActivity(intent);
                        }
                    });
                    recyclerView.setAdapter(adapter);
                }
            }

            @Override
            public void onFailure(Call<TVShow> call, Throwable t) {

            }
        });

    }

    private void getMovie(Movie movie){

        Picasso.get().load(Constants.BASE_IMAGE_URL + movie.getPoster_path()).into(imageView);

        name.setText(movie.getTitle());
        rating.setText(String.valueOf(movie.getVote_average()));
        release_date.setText(movie.getRelease_date());
        overview.setText(movie.getOverview());
        recyclerView.setVisibility(View.GONE);

        ratingBar.setRating((float) (movie.getPopularity() / 50));

    }

    private void getKnownFor(final KnownFor knownFor){

        if (knownFor.getMedia_type().equals("movie")){

            Picasso.get().load(Constants.BASE_IMAGE_URL + knownFor.getPoster_path()).into(imageView);

            name.setText(knownFor.getTitle());
            rating.setText(String.valueOf(knownFor.getVote_average()));
            release_date.setText(knownFor.getRelease_date());
            overview.setText(knownFor.getOverview());
            recyclerView.setVisibility(View.GONE);

            ratingBar.setRating((float) (knownFor.getPopularity() / 50));
        }

        if (knownFor.getMedia_type().equals("tv")){

            Picasso.get().load(Constants.BASE_IMAGE_URL + knownFor.getPoster_path()).into(imageView);

            name.setText(knownFor.getName());
            rating.setText(String.valueOf(knownFor.getVote_average()));
            release_date.setText(knownFor.getRelease_date());
            overview.setText(knownFor.getOverview());

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Movie_TVShow_Profile.this, LinearLayoutManager.HORIZONTAL, false);
            recyclerView.setLayoutManager(linearLayoutManager);
            recyclerView.setHasFixedSize(true);

            final Endpoints endpoints = ApiUtils.getApiService();

            endpoints.getTVShow(knownFor.getId(), Constants.API_KEY, "en-US", 1).enqueue(new Callback<TVShow>() {
                @Override
                public void onResponse(@NonNull Call<TVShow> call, @NonNull Response<TVShow> response) {
                    if (response.isSuccessful() && response.body() != null){
                        List<TVSeason> seasons = response.body().getSeasons();
                        adapter = new SeasonsAdapter(Movie_TVShow_Profile.this, seasons);
                        adapter.setOnClickListener(new SeasonsAdapter.onClickListener() {
                            @Override
                            public void onClick(int season_number) {
                                Intent intent = new Intent(Movie_TVShow_Profile.this, TVShowSeason.class);
                                intent.putExtra("tv_id", knownFor.getId());
                                intent.putExtra("season_number", season_number);
                                intent.putExtra("tvshow_name", tvShow.getName());
                                startActivity(intent);
                            }
                        });
                        recyclerView.setAdapter(adapter);
                    }
                }

                @Override
                public void onFailure(@NonNull Call<TVShow> call, @NonNull Throwable t) {

                }
            });

            ratingBar.setRating((float) (knownFor.getPopularity() / 40));

        }

    }

}
