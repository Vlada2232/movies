package com.instagram.vlada.movies.Activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Episode;
import com.instagram.vlada.movies.model.Guest_star;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EpisodeProfile extends AppCompatActivity {

    private ImageView imageView;
    private TextView name, air_date, overview, guest_stars, episode_number;
    private Episode episode;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_episode_profile);

        imageView = findViewById(R.id.image);
        name = findViewById(R.id.name);
        air_date = findViewById(R.id.air_date);
        overview = findViewById(R.id.overview);
        guest_stars = findViewById(R.id.guest_stars);
        episode_number = findViewById(R.id.episode_number);

        episode = (Episode) getIntent().getSerializableExtra("episode");
        position = getIntent().getIntExtra("episode_number", 0);

        position = position + 1;

        name.setText(episode.getName());
        air_date.setText(episode.getAir_date());
        overview.setText(episode.getOverview());
        episode_number.setText("Episode: " + position);

        StringBuilder builder = new StringBuilder();
        for (Guest_star guest_star : episode.getGuest_stars()){
            builder.append(guest_star.getName());
            builder.append(" ");
        }

        guest_stars.setText("Guest stars: " + builder);

        Picasso.get().load(Constants.BASE_IMAGE_URL + episode.getStill_path()).into(imageView);

    }

}
