package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

public class Genre {

    @SerializedName("id")
    private double id;

    @SerializedName("name")
    private String name;

    public Genre(double id, String name) {
        this.id = id;
        this.name = name;
    }

    public double getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
