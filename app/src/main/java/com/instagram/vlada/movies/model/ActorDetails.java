package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActorDetails {

    @SerializedName("birthday")
    private String birthday;

    @SerializedName("known_for_department")
    private String known_for_department;

    @SerializedName("deathday")
    private String deathday;

    @SerializedName("id")
    private long id;

    @SerializedName("name")
    private String name;

    @SerializedName("also_known_as")
    private List also_known_as;

    @SerializedName("gender")
    private int gender;

    @SerializedName("biography")
    private String biography;


    @SerializedName("place_of_birth")
    private String place_of_birth;

    @SerializedName("profile_path")
    private String profile_path;

    @SerializedName("adult")
    private boolean adult;

    @SerializedName("imdb_id")
    private String imdb_id;

    @SerializedName("homepage")
    private String homepage;


    public ActorDetails(String birthday, String known_for_department, String deathday, long id, String name, List also_known_as, int gender, String biography, String place_of_birth, String profile_path, boolean adult, String imdb_id, String homepage) {
        this.birthday = birthday;
        this.known_for_department = known_for_department;
        this.deathday = deathday;
        this.id = id;
        this.name = name;
        this.also_known_as = also_known_as;
        this.gender = gender;
        this.biography = biography;
        this.place_of_birth = place_of_birth;
        this.profile_path = profile_path;
        this.adult = adult;
        this.imdb_id = imdb_id;
        this.homepage = homepage;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getKnown_for_department() {
        return known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public String getDeathday() {
        return deathday;
    }

    public void setDeathday(String deathday) {
        this.deathday = deathday;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List getAlso_known_as() {
        return also_known_as;
    }

    public void setAlso_known_as(List also_known_as) {
        this.also_known_as = also_known_as;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public String getHomepage() {
        return homepage;
    }

    public void setHomepage(String homepage) {
        this.homepage = homepage;
    }
}
