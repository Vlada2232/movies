package com.instagram.vlada.movies.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Actor  {

    @SerializedName("popularity")
    private double popularity;

    @SerializedName("id")
    private double id;

    @SerializedName("profile_path")
    private String profile_path;

    @SerializedName("name")
    private String name;

    @SerializedName("known_for")
    private List<KnownFor> known_for;

    @SerializedName("adult")
    private boolean adult;

    public Actor(double popularity, double id, String profile_path, String name, List<KnownFor> known_for, boolean adult) {
        this.popularity = popularity;
        this.id = id;
        this.profile_path = profile_path;
        this.name = name;
        this.known_for = known_for;
        this.adult = adult;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public double getId() {
        return id;
    }

    public void setId(double id) {
        this.id = id;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<KnownFor> getKnown_for() {
        return known_for;
    }

    public void setKnown_for(List<KnownFor> known_for) {
        this.known_for = known_for;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }
}
