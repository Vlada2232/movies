package com.instagram.vlada.movies.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.Adapters.ActorProfileAdapter;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.ActorDetails;
import com.instagram.vlada.movies.model.ActorsResponse;
import com.instagram.vlada.movies.model.KnownFor;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActorProfile extends AppCompatActivity {

    private ActorDetails actorDetails;
    private List<KnownFor> knownForList;
    private TextView name, birthday, placeOfBirth, biography, AKA;
    private ImageView imageView;
    private double id;
    private int position;
    private ActorProfileAdapter adapter;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actor_profile);

        name = findViewById(R.id.name);
        birthday = findViewById(R.id.birthday);
        placeOfBirth = findViewById(R.id.place_of_birth);
        biography = findViewById(R.id.biography);
        AKA = findViewById(R.id.also_known_as);
        imageView = findViewById(R.id.actor_profile_image);

        id = getIntent().getDoubleExtra("id", 0);
        position = getIntent().getIntExtra("position", 0);

        final ProgressDialog progressDialog = ProgressDialog.show(ActorProfile.this, "", "Loading. Please wait...");

        final Endpoints endpoints = ApiUtils.getApiService();
        endpoints.getActorDetails(id, Constants.API_KEY, "en-US", 1).enqueue(new Callback<ActorDetails>() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onResponse(@NonNull Call<ActorDetails> call, @NonNull Response<ActorDetails> response) {
                if (response.isSuccessful() && response.body() != null){
                    actorDetails = response.body();
                    if (actorDetails != null)
                        initView(actorDetails);

                    endpoints.getActors(Constants.API_KEY, "en-US", "1").enqueue(new Callback<ActorsResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<ActorsResponse> call, @NonNull Response<ActorsResponse> response) {
                            if (response.isSuccessful() && response.body().getResults() != null){
                                knownForList = response.body().getResults().get(position).getKnown_for();
                                adapter = new ActorProfileAdapter(ActorProfile.this, knownForList);
                                adapter.setOnItemClickListener(new ActorProfileAdapter.onItemClickListener() {
                                    @Override
                                    public void onClick(KnownFor knownFor) {
                                        Intent intent = new Intent(ActorProfile.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "KnownFor");
                                        intent.putExtra("KnownFor", knownFor);
                                        startActivity(intent);
                                    }
                                });
                                recyclerView.setAdapter(adapter);
                                progressDialog.dismiss();
                                if (call.isCanceled() || call.isExecuted()){
                                    progressDialog.dismiss();
                                }
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<ActorsResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });
                }
                else {
                    Toast.makeText(ActorProfile.this, response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ActorDetails> call, @NonNull Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ActorProfile.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });

        recyclerView = findViewById(R.id.actor_profile_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        recyclerView.setLayoutManager(layoutManager);

    }

    private void initView(ActorDetails actorDetails) {

        name.setText(actorDetails.getName());
        if (actorDetails.getBiography().equals("")){
            biography.setText(R.string.no_biograpfhy_available);
        }else {
            biography.setText(actorDetails.getBiography());
        }
        if (actorDetails.getBirthday() == null){
            birthday.setText(R.string.no_birthday_available);
        }
        else {
            birthday.setText(actorDetails.getBirthday());
        }
        birthday.setText(actorDetails.getBirthday());
        if (actorDetails.getPlace_of_birth() == null){
            placeOfBirth.setText(R.string.place_of_birth_not_available);
        }else {
            placeOfBirth.setText(actorDetails.getPlace_of_birth());
        }

        List<String> akaList = actorDetails.getAlso_known_as();

        StringBuilder builder = new StringBuilder();
        for (String name: akaList) {
            builder.append(name).append(" ");
        }

        if (akaList.isEmpty()){
            AKA.setText("AKA: " + R.string.not_available);
        }else {
            AKA.setText("AKA: " + builder);
        }



        Picasso.get().load(Constants.BASE_IMAGE_URL + actorDetails.getProfile_path()).into(imageView);

    }

}
