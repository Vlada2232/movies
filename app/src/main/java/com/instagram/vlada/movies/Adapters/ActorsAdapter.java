package com.instagram.vlada.movies.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Actor;
import com.instagram.vlada.movies.model.KnownFor;
import com.instagram.vlada.movies.model.Movie;
import com.instagram.vlada.movies.model.TVShow;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ActorsAdapter extends RecyclerView.Adapter<ActorsAdapter.ViewHolder> {

    private Context context;
    private List<Actor> actors;
    private List<Movie> movies;
    private List<TVShow> tvShows;

    public ActorsAdapter(Context context, List<Actor> actors, List<Movie> movies, List<TVShow> tvShows) {
        this.context = context;
        this.actors = actors;
        this.movies = movies;
        this.tvShows = tvShows;
    }

    @Override
    public ActorsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflator = LayoutInflater.from(context);
        @SuppressLint("InflateParams") View view = inflator.inflate(R.layout.list_adapter_view, null);
        final ViewHolder holder = new ViewHolder(view);

        return holder;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(ActorsAdapter.ViewHolder holder, final int position) {
        final Actor actor = actors.get(position);

       List<KnownFor> known_for_list = actor.getKnown_for();

        List<String> namesMovie = new ArrayList<>();
        for (KnownFor knownFor : known_for_list) {
            if (knownFor.getTitle() == null)
            {
                namesMovie.add(knownFor.getName());
            }else {
                namesMovie.add(knownFor.getTitle());
            }

        }



        String knownFor = "Known For:";

        StringBuilder builder = new StringBuilder();
        for (String name: namesMovie) {
            builder.append(name).append(System.getProperty("line.separator"));
        }

        holder.knownFor.setText(spannableStringBuilder(knownFor, builder, context));
        holder.knownFor.setTextColor(Color.WHITE);
        holder.name.setText(actor.getName());
        holder.name.setTextColor(Color.WHITE);
        holder.genre.setVisibility(View.GONE);
        holder.rating.setVisibility(View.GONE);
        holder.ratingg.setVisibility(View.GONE);
        holder.actor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onActorClikcListener.onClick(actor.getId(), position);
            }
        });

        Picasso.get().load(Constants.BASE_IMAGE_URL + actor.getProfile_path()).into(holder.profImage);

    }

    @Override
    public int getItemCount() {
        return actors.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, rating, ratingg, genre, knownFor;
        ImageView profImage;
        LinearLayout actor;

        ViewHolder(View itemView) {
            super(itemView);

//            name = itemView.findViewById(R.id.name);
//            profImage = itemView.findViewById(R.id.movie_image);
//            ratingg = itemView.findViewById(R.id.ratingg);
//            rating = itemView.findViewById(R.id.rating);
//            genre = itemView.findViewById(R.id.genre);
//            knownFor = itemView.findViewById(R.id.known_for);
//            actor = itemView.findViewById(R.id.linearLayout);

        }
    }

    private static SpannableStringBuilder spannableStringBuilder(String knownFor, StringBuilder builder, Context context) {

        SpannableStringBuilder stringBuilder = new SpannableStringBuilder();

        SpannableString string = new SpannableString(knownFor);
        StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
        string.setSpan(boldSpan, 0, knownFor.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        string.setSpan(new ForegroundColorSpan(ContextCompat.getColor(context, R.color.white)), 0, knownFor.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        stringBuilder.append(string);

        stringBuilder.append(System.getProperty("line.separator"));

        stringBuilder.append(builder);


        return stringBuilder;
    }

    private onActorClikcListener onActorClikcListener;
    public interface onActorClikcListener {

        void onClick(double id, int position);

    }

    public void setOnActorClikcListener(ActorsAdapter.onActorClikcListener onActorClikcListener) {
        this.onActorClikcListener = onActorClikcListener;
    }
}
