package com.instagram.vlada.movies.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.instagram.vlada.Endpoints;
import com.instagram.vlada.movies.Adapters.TVshowsAdapter;
import com.instagram.vlada.movies.R;
import com.instagram.vlada.movies.Utils.ApiUtils;
import com.instagram.vlada.movies.Utils.Constants;
import com.instagram.vlada.movies.model.Genre;
import com.instagram.vlada.movies.model.GenreResponse;
import com.instagram.vlada.movies.model.TVShow;
import com.instagram.vlada.movies.model.TVShowResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TV_Shows extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<TVShow> tvShows;
    private List<Genre> genres;
    private TVshowsAdapter tVshowsAdapter;
    private int page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv__shows);

        final Endpoints endpoints = ApiUtils.getApiService();

        final ProgressDialog progressDialog = ProgressDialog.show(TV_Shows.this, String.valueOf(page), "Loading. Please wait...");

        endpoints.getPopularTVShows(Constants.API_KEY, "en-US", "1", "").enqueue(new Callback<TVShowResponse>() {
            @Override
            public void onResponse(@NonNull Call<TVShowResponse> call, @NonNull Response<TVShowResponse> response) {
                if (response.isSuccessful() && response.body().getResults() != null){

                    tvShows = response.body().getResults();

                    endpoints.getGenre(Constants.API_KEY, "en-US").enqueue(new Callback<GenreResponse>() {
                        @Override
                        public void onResponse(@NonNull Call<GenreResponse> call, @NonNull Response<GenreResponse> response) {
                            if (response.isSuccessful() && response.body().getGenres() != null){
                                genres = response.body().getGenres();
                                tVshowsAdapter = new TVshowsAdapter(TV_Shows.this, tvShows, genres);
                                recyclerView.setAdapter(tVshowsAdapter);
                                progressDialog.dismiss();
                                tVshowsAdapter.setOnItemClickListener(new TVshowsAdapter.onItemClickListener() {
                                    @Override
                                    public void OnItemClick(TVShow tvShow) {
                                        Intent intent = new Intent(TV_Shows.this, Movie_TVShow_Profile.class);
                                        intent.putExtra("whatScreen", "TvShow");
                                        intent.putExtra("TvShow", tvShow);
                                        startActivity(intent);
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(@NonNull Call<GenreResponse> call, @NonNull Throwable t) {
                            progressDialog.dismiss();
                        }
                    });

                }
            }

            @Override
            public void onFailure(@NonNull Call<TVShowResponse> call, @NonNull Throwable t) {
                progressDialog.dismiss();
            }
        });

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(layoutManager);
    }

}
