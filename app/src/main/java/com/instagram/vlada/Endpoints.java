package com.instagram.vlada;

import com.instagram.vlada.movies.model.Actor;
import com.instagram.vlada.movies.model.ActorDetails;
import com.instagram.vlada.movies.model.ActorsResponse;
import com.instagram.vlada.movies.model.Episode;
import com.instagram.vlada.movies.model.GenreResponse;
import com.instagram.vlada.movies.model.MovieResponse;
import com.instagram.vlada.movies.model.TVSeason;
import com.instagram.vlada.movies.model.TVShow;
import com.instagram.vlada.movies.model.TVShowResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Endpoints {

    @GET("/3/movie/popular")
    Call<MovieResponse> getFavouriteMovies(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page,
            @Query("region") String region

    );

    @GET("/3/movie/top_rated")
    Call<MovieResponse> getTopRatedMovies(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page,
            @Query("region") String region

    );

    @GET("/3/movie/latest")
    Call<MovieResponse> getLatestMovies(

            @Query("api_key") String api_key,
            @Query("language") String language

    );
    @GET("/3/movie/now_playing")
    Call<MovieResponse> getNowPlayingMovies(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page,
            @Query("region") String region

    );

    @GET("/3/movie/upcoming")
    Call<MovieResponse> getUpcomingMovies (

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page,
            @Query("region") String region

    );

    @GET("/3/genre/movie/list")
    Call<GenreResponse> getGenre (

            @Query("api_key") String api_key,
            @Query("language") String language

    );

    @GET("/3/tv/popular")
    Call<TVShowResponse> getPopularTVShows(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page,
            @Query("region") String region

    );

    @GET("/3/tv/top_rated")
    Call<TVShowResponse> getTopRatedTVShows(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page

    );

    @GET("/3/tv/latest")
    Call<TVShowResponse> getlatestTVShows(

            @Query("api_key") String api_key,
            @Query("language") String language

    );

    @GET("/3/tv/airing_today")
    Call<TVShowResponse> getAiringTodayTVShows(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page

    );

    @GET("/3/tv/on_the_air")
    Call<TVShowResponse> getOnTheAirTVShows (

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page

    );

    @GET("/3/person/popular")
    Call<ActorsResponse> getActors(

            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("page") String page

    );

    @GET("/3/person/{person_id}")
    Call<ActorDetails> getActorDetails(

            @Path("person_id") double person_id,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("append_to_response") int append_to_response

    );

    @GET("/3/tv/{tv_id}")
    Call<TVShow> getTVShow(

            @Path("tv_id") double tv_id,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("append_to_response") int append_to_response

    );

    @GET("/3/tv/{tv_id}/season/{season_number}")
    Call<TVSeason> getTvSeason (

            @Path("tv_id") double tv_id,
            @Path("season_number") int season_number,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("append_to_response") int append_to_response

    );

    @GET("/3/tv/{tv_id}/season/{season_number}/episode/{episode_number}")
    Call<Episode> getEpisode (

            @Path("tv_id") double tv_id,
            @Path("season_number") int season_number,
            @Path("episode_number") int episode_number,
            @Query("api_key") String api_key,
            @Query("language") String language,
            @Query("append_to_response") int append_to_response

    );



}
